<?php
defined('BASEPATH') or exit('No direct script access allowed');


$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

// ============ ROUTE API CLIENT ============ 
$route['api/client']['GET']                 = 'C_client/get_all';
$route['api/client/lawyer']['GET']          = 'C_client/get_by_lawyer';
$route['api/client/(:num)']['GET']          = 'C_client/get/$1';
$route['api/client']['POST']                = 'C_client/register';
$route['api/client/update']['POST']         = 'C_client/update';
$route['api/client/(:num)']['DELETE']         = 'C_client/delete/$1';

// ============ ROUTE API LOGIN ============ 
$route['api/login']                         = 'C_login/auth';

// ============ ROUTE API WILAYAH ============ 
$route['api/wilayah/provinsi']['GET']                          = 'C_wilayah/get_all';
$route['api/wilayah/kabupaten/(:num)']['GET']         = 'C_wilayah/get_kabupaten/$1';
$route['api/wilayah/kecamatan/(:num)']['GET']         = 'C_wilayah/get_kecamatan/$1';
$route['api/wilayah/kelurahan/(:num)']['GET']         = 'C_wilayah/get_kelurahan/$1';