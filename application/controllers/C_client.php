<?php
defined('BASEPATH') or exit('No direct script access allowed');

class C_client extends CI_Controller
{

    function __construct()
    {
        // ALLOWING CORS
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE');
        header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');
        // $method = $_SERVER['REQUEST_METHOD'];
        // if($method == "OPTIONS") {
        //     die();
        // }
        parent::__construct();

        $this->load->model('M_client');
    }

    public function response($data)
    {
        $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
            ->_display();

        exit;
    }

    public function register()
    {
        return $this->response($this->M_client->save_api());
    }

    public function update()
    {
        return $this->response($this->M_client->update());
    }

    public function get_all()
    {
        return $this->response($this->M_client->get());
    }

    public function get_by_lawyer()
    {
        $id_lawyer = $this->input->get('id_lawyer');
        $status = $this->input->get('status');
        return $this->response($this->M_client->get('lawyer_id', $id_lawyer, $status));
        // var_dump($id_lawyer, $status);
    }

    public function get($id)
    {
        return $this->response($this->M_client->get('id_client', $id));
    }

    public function delete($id)
    {
        return $this->response($this->M_client->delete('id_client', $id));
    }
}
