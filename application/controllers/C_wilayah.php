<?php
defined('BASEPATH') or exit('No direct script access allowed');

class C_wilayah extends CI_Controller
{

    function __construct()
    {
        // ALLOWING CORS
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE');
        header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');
        // $method = $_SERVER['REQUEST_METHOD'];
        // if($method == "OPTIONS") {
        //     die();
        // }
        parent::__construct();

        $this->load->model('M_wilayah');
    }

    public function response($data)
    {
        $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
            ->_display();

        exit;
    }

    public function register()
    {
        return $this->response($this->M_wilayah->save_api());
    }

    public function update()
    {
        return $this->response($this->M_wilayah->update());
    }

    public function get_all()
    {
        return $this->response($this->M_wilayah->get());
    }

    public function get_kabupaten($provinsi_id)
    {
        return $this->response($this->M_wilayah->get('kab_propinsi_id', $provinsi_id));
    }

    public function get_kecamatan($kabupaten_id)
    {
        return $this->response($this->M_wilayah->get('kec_kab_id', $kabupaten_id));
    }

    public function get_kelurahan($kecamatan_id)
    {
        return $this->response($this->M_wilayah->get('kel_kec_id', $kecamatan_id));
    }

    public function delete($id)
    {
        return $this->response($this->M_wilayah->delete('id_wilayah', $id));
    }
}
