<?php
class M_client extends CI_Model
{

    private $_table = "l_client_member";
    private $_lawyer = "l_member_detail";
    private $_bakum = "l_badan_hukum";

    public function save_api()
    {
        
        if ($this->input->post('id_client') == null) {
            for ($i=0; $i < count($_FILES['client_ktp_scan']['name']); $i++) { 
                $file_name = $_FILES['client_ktp_scan']['name'][$i];
                $tmp_name = $_FILES['client_ktp_scan']['tmp_name'][$i];

                // return $file_name;

                $data = [
                    'client_name' => $this->input->post('client_name'),
                    'client_email' => $this->input->post('client_email'),
                    'client_ktp' => $this->input->post('client_ktp'),
                    'client_phone' => $this->input->post('client_phone'),
                    'client_address' => $this->input->post('client_address'),
                    'province' => $this->input->post('province'),
                    'kab_kota' => $this->input->post('kab_kota'),
                    'kecamatan' => $this->input->post('kecamatan'),
                    'kelurahan' => $this->input->post('kelurahan'),
                    'avatar' => $this->input->post('avatar'),
                    'client_ktp_scan' => $file_name,
                    'lawyer_id' => $this->input->post('lawyer_id'),
                    'created_at' => $this->input->post('created_at'),
                    'edited_at' => $this->input->post('edited_at'),
                    'password' => $this->input->post('password'),
                    'status' => $this->input->post('status'),
                ];
        
        
                if ($this->db->insert($this->_table, $data)) {
                    if ( !is_dir("media/attachment/client/".$this->db->insert_id())){
                        mkdir("media/attachment/client/".$this->db->insert_id(), 0777, true);
                    }
                    
                    move_uploaded_file($tmp_name, "media/attachment/client/".$this->db->insert_id()."/".$file_name);
                    return [
                        'id_client' => $this->db->insert_id(),
                        'success'   => true,
                        'message'   => 'Data Berhasil Tersimpan'
                    ];
                }
            }
        } else {
            $data = [
                'client_id' => $this->input->post('id_client'),
                'nama_bakum' => $this->input->post('nama_bakum'),
                'alamat_bakum' => $this->input->post('alamat_bakum'),
                'provinsi_bakum' => $this->input->post('provinsi_bakum'),
                'kab_kota_bakum' => $this->input->post('kab_kota_bakum'),
                'kecamatan_bakum' => $this->input->post('kecamatan_bakum'),
                'kelurahan_bakum' => $this->input->post('kelurahan_bakum'),
                'no_akta_bakum' => $this->input->post('no_akta_bakum'),
                'tgl_akta_bakum' => $this->input->post('tgl_akta_bakum'),
                'nama_notaris_bakum' => $this->input->post('nama_notaris_bakum'),
                'sk_menkumham_bakum' => $this->input->post('sk_menkumham_bakum'),
                'dokumen_bakum' => $this->input->post('dokumen_bakum'),
            ];
    
    
            if ($this->db->insert($this->_bakum, $data)) {
                return [
                    'id_bakum' => $this->db->insert_id(),
                    'success'   => true,
                    'message'   => 'Data Berhasil Tersimpan'
                ];
            }
        }
    }

    public function get($key = null, $value = null, $status = null)
    {
        if ($key != null && $key == 'id_client') {
            $this->db->select('*');
            $this->db->select('l_client_member.password as client_password');
            $this->db->select('l_client_member.province as client_provinsi');
            $this->db->select('l_client_member.kab_kota as client_kabupaten');
            $this->db->select('l_client_member.kecamatan as client_kecamatan');
            $this->db->select('l_client_member.kelurahan as client_kelurahan');
            $this->db->from($this->_table);
            $this->db->join($this->_lawyer, 'l_member_detail.id = l_client_member.lawyer_id');
            $this->db->join($this->_bakum, 'l_badan_hukum.client_id = l_client_member.id_client');
            $this->db->where($key, $value);
            $query = $this->db->get();
            return $query->row();

        } elseif ($key != null && $key == 'lawyer_id' && $status == '1') {
            $this->db->select('*');
            $this->db->select('l_client_member.password as client_password');
            $this->db->from($this->_table);
            $this->db->join('l_member_detail', 'l_member_detail.id = l_client_member.lawyer_id');
            $this->db->where($key, $value);
            $this->db->where('l_client_member.status', $status);
            $query = $this->db->get();
            return $query->result();

        } elseif ($key != null && $key == 'lawyer_id' && $status == '2') {
            $this->db->select('*');
            $this->db->select('l_client_member.password as client_password');
            $this->db->from($this->_table);
            $this->db->join('l_member_detail', 'l_member_detail.id = l_client_member.lawyer_id');
            $this->db->where($key, $value);
            $this->db->where('l_client_member.status', $status);
            $query = $this->db->get();
            return $query->result();
        }

        $this->db->select('*');
        $this->db->select('l_client_member.password as client_password');
        $this->db->from($this->_table);
        $this->db->join('l_member_detail', 'l_member_detail.id = l_client_member.lawyer_id');
        $query = $this->db->get();
        return $query->result();
    }

    public function delete($key, $value)
    {
        if ($key != null) {
            $query = $this->db->delete($this->_table, array($key => $value));
            $query = $this->db->delete($this->_bakum, array('client_id' => $value));
            return $query;
        }
    }

    public function update()
    {
        for ($i=0; $i < count($_FILES['client_ktp_scan']['name']); $i++) { 
            $file_name = $_FILES['client_ktp_scan']['name'];
            $tmp_name = $_FILES['client_ktp_scan']['tmp_name'];
            $client = [
                'id_client' => $this->input->post('id_client'),
                'client_name' => $this->input->post('client_name'),
                'client_email' => $this->input->post('client_email'),
                'client_ktp' => $this->input->post('client_ktp'),
                'client_phone' => $this->input->post('client_phone'),
                'client_address' => $this->input->post('client_address'),
                'province' => $this->input->post('province'),
                'kab_kota' => $this->input->post('kab_kota'),
                'kecamatan' => $this->input->post('kecamatan'),
                'kelurahan' => $this->input->post('kelurahan'),
                'avatar' => $this->input->post('avatar'),
                'client_ktp_scan' => $file_name,
                'lawyer_id' => $this->input->post('lawyer_id'),
                'created_at' => $this->input->post('created_at'),
                'edited_at' => $this->input->post('edited_at'),
                'password' => $this->input->post('password'),
                'status' => $this->input->post('status'),
            ];

            $ret = $this->db->update($this->_table, $client, array('id_client' => $this->input->post('id_client')));

            $bakum = [
                'id_bakum' => $this->input->post('id_bakum'),
                'nama_bakum' => $this->input->post('nama_bakum'),
                'alamat_bakum' => $this->input->post('alamat_bakum'),
                'provinsi_bakum' => $this->input->post('provinsi_bakum'),
                'kab_kota_bakum' => $this->input->post('kab_kota_bakum'),
                'kecamatan_bakum' => $this->input->post('kecamatan_bakum'),
                'kelurahan_bakum' => $this->input->post('kelurahan_bakum'),
                'no_akta_bakum' => $this->input->post('no_akta_bakum'),
                'tgl_akta_bakum' => $this->input->post('tgl_akta_bakum'),
                'nama_notaris_bakum' => $this->input->post('nama_notaris_bakum'),
                'sk_menkumham_bakum' => $this->input->post('sk_menkumham_bakum'),
                'dokumen_bakum' => $this->input->post('dokumen_bakum'),
            ];

            $ret = $this->db->update($this->_bakum, $bakum, array('client_id' => $this->input->post('id_client')));

            if ($ret) {
                return [
                    'success'   => true,
                    'message'   => 'Data Berhasil Diupdate'
                ];
            }
        }

    }
}
